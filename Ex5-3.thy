theory "Ex5-3" imports Main begin
  
(* Exercise 5.3. Give a structured proof by rule inversion:
lemma assumes a: "ev(Suc(Suc n))" shows "ev n" *)

inductive ev :: "nat \<Rightarrow> bool" where
ev0: "ev 0" |
evSS: "ev n \<Longrightarrow> ev (Suc (Suc n))"

lemma assumes a: "ev(Suc(Suc n))" shows "ev n"
proof -
  show ?thesis using a
  proof cases
    case evSS
    then show ?thesis by auto
  qed
qed

end