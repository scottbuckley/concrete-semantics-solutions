theory Ex77 imports Small_Step begin



lemma disj_or_back : "(\<not>B \<Longrightarrow> A) \<Longrightarrow> (A \<or> B)"
  by auto

thm spec

lemma seq_ss: "(c1;;c2, s1) \<rightarrow> (c3, s2) \<Longrightarrow> c1 \<noteq> SKIP \<Longrightarrow> (\<exists> c1'. c3=c1';;c2)"
  by (induct "c1;;c2" _ _ _ rule:small_step_induct; auto)

theorem ex77: "\<lbrakk> (C::nat \<Rightarrow> com) 0 = c;;d ; \<forall>n.(C n, S n) \<rightarrow> (C (Suc n), S(Suc n))\<rbrakk> \<Longrightarrow>
(
  \<forall>n. \<exists>c1 c2.
    C n = c1;;d \<and>
    C (Suc n) = c2;;d \<and>
    (c1, S n) \<rightarrow> (c2, S(Suc n))
)
\<or> (\<exists>k. C k = SKIP;;d)"
  apply (rule disj_or_back)
  apply auto
  apply (induct_tac n)
  apply (rule exI [where x=c])
  apply simp
  apply (frule spec [where x=0])
  apply (clarsimp)
  apply (rotate_tac 3)
  apply (induct "c;;d" _ _ _ rule:small_step_induct)
  apply clarsimp (* eliminates from c=SKIP *)
  apply clarsimp (* solves base case *)
  apply clarsimp
  apply (drule_tac x="Suc na" in spec)
  apply (rotate_tac 5)
  apply simp
  apply (frule seq_ss)
  apply auto
  done




















end