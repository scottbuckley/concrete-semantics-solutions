theory Ex7 imports Big_Step begin

(* 7.1 *)
fun assigned :: "com \<Rightarrow> vname set" where
"assigned SKIP = {}" |
"assigned (Assign v _) = {v}" |
"assigned (Seq c1 c2) = (assigned c1) \<union> (assigned c2)" |
"assigned (If _ c1 c2) = (assigned c1) \<union> (assigned c2)" |
"assigned (While _ c) = assigned c"

lemma "(c, s) \<Rightarrow> t \<Longrightarrow> x \<notin> assigned c \<Longrightarrow> s x = t x"
  apply (induction rule:big_step_induct)
        apply auto
  done




(* 7.2 *)
fun skip :: "com \<Rightarrow> bool" where
"skip SKIP = True" |
"skip (Assign _ _) = False" |
"skip (Seq c1 c2) = ((skip c1) \<and> (skip c2))" |
"skip (If x c1 c2) = ((skip c1) \<and> (skip c2))" |
"skip (While _ c) = False"

lemma skip_steps: "skip c \<Longrightarrow> (c, s) \<Rightarrow> s"
  apply (induction c arbitrary:s rule:skip.induct)
      apply auto
  done

lemma "skip c \<Longrightarrow> c \<sim> SKIP"
  apply (induction c rule:skip.induct)
      apply auto
   apply (rule skip_steps) apply auto
  apply (case_tac "bval x s")
   apply (rule IfTrue; auto)
  apply (rule IfFalse; auto)
  done


(* 7.3 *)
fun deskip :: "com \<Rightarrow> com" where
"deskip SKIP = SKIP" |
"deskip (Assign a b) = Assign a b" |
"deskip (Seq c1 c2) = (case ((deskip c1), (deskip c2)) of
  (SKIP, z2)    \<Rightarrow> z2 |
  (z1, SKIP)    \<Rightarrow> z1 |
  (z1, z2)       \<Rightarrow> (Seq z1 z2)
)" |
"deskip (If b c1 c2) = (case (deskip c1, deskip c2) of
  (SKIP, SKIP) \<Rightarrow> SKIP |
  (z1, z2)     \<Rightarrow> If b z1 z2
)" |
"deskip (While b c) = While b (deskip c)"


lemma equiv_seq: "c1 \<sim> z1 \<Longrightarrow> c2 \<sim> z2 \<Longrightarrow> (c1 ;; c2) \<sim> (z1 ;; z2)"
  apply auto
  done

lemma deskip_seq: "(deskip (c1 ;; c2) \<sim> (deskip c1 ;; deskip c2))"
  apply (case_tac "deskip c1"; case_tac "deskip c2"; auto)
  done

lemma deskip_if: "(deskip (If b c1 c2) \<sim> If b (deskip c1) (deskip c2))"
  apply (case_tac "deskip c1"; case_tac "deskip c2"; auto)
  done

lemma deskip_while: "(deskip (While b c) \<sim> While b (deskip c))"
  apply (case_tac "deskip c"; auto)
  done


lemma "deskip c \<sim> c"
  apply (induction c)
  apply simp
  apply simp
  apply (rule sim_trans [OF deskip_seq]) apply (rule equiv_seq; simp)
  apply (rule sim_trans [OF deskip_if])  apply (auto)[1]
  apply (rule sim_trans [OF deskip_while])
  apply (rule sim_while_cong; simp)
  done


(* 7.4 *)
inductive astep :: "aexp \<times> state \<Rightarrow> aexp \<Rightarrow> bool" (infix "\<leadsto>" 50) where
"(V x , s) \<leadsto> N (s x )" |
"(Plus (N i) (N j), s) \<leadsto> N (i + j)" |
"(b, s) \<leadsto> b' \<Longrightarrow> (Plus (N i) b, s) \<leadsto> Plus (N i) b'" |
"(a, s) \<leadsto> a' \<Longrightarrow> (Plus a b, s) \<leadsto> Plus a' b"

theorem "(a, s) \<leadsto> a' \<Longrightarrow> aval a s = aval a' s"
  apply (induction rule:astep.induct[split_format (complete)])
  apply auto
  done



(* 7.5 *)
lemma "IF And b1 b2 THEN c1 ELSE c2 \<sim> IF b1 THEN IF b2 THEN c1 ELSE c2 ELSE c2"
  apply auto apply auto
  done

abbreviation
  nonterm :: "com \<Rightarrow> state \<Rightarrow> bool" where
  "nonterm c s \<equiv> (\<forall>t. \<not>((c,s) \<Rightarrow> t))"


lemma whileTrue_const_false : "(While (Bc True) c, s1) \<Rightarrow> s2 \<Longrightarrow> False"
  apply (induction "While (Bc True) c" _ _ rule:big_step_induct)
   apply auto
  done


lemma whileTrue_false_aux : "(While b c, s1) \<Rightarrow> s2 \<Longrightarrow> All (bval b) \<Longrightarrow> False"
  apply (induction "While b c" _ _ rule:big_step_induct)
   apply auto
  done

lemma whileTrue_false_allb : "All (bval b) \<Longrightarrow> (While b c, s1) \<Rightarrow> s2  \<Longrightarrow> False"
  apply (rule whileTrue_false_aux; auto)
  done

lemma whileTrue_false : "(While (Bc True) c, s1) \<Rightarrow> s2  = False"
  apply auto
  apply (rule whileTrue_false_allb; auto)
  done

lemma whileTrue_nonterm : "(\<forall> q. bval b q = True) \<Longrightarrow> nonterm (While b c) s"
  apply auto
  apply (rule whileTrue_false_allb; auto)
  done

lemma while_skip_cong : "(\<forall> q. bval b q = False) \<Longrightarrow> ((While b c) \<sim> SKIP)"
  apply auto
  done

(* lemma osaux1 : "((While b c) \<sim> SKIP)" *)
  (* apply auto *)
  (* done *)

lemma demorg1: "(\<exists>x. \<not>P(x)) = (\<not>(\<forall>x. P(x)))"
  apply auto
  done

lemma "\<not>(\<forall> b1 b2. WHILE And b1 b2 DO c \<sim> WHILE b1 DO WHILE b2 DO c)"
  apply (subst demorg1[symmetric])
  apply (subst demorg1[symmetric])
  apply (rule exI [where x="Bc True"])
  apply (rule exI [where x="Bc False"])
  apply (subst whileTrue_false)
  apply auto
  apply (rule exI; rule exI)
  apply (rule WhileFalse) apply auto
  done

abbreviation
  Or :: "bexp \<Rightarrow> bexp \<Rightarrow> bexp" where
  "Or b1 b2 \<equiv> Not (And (Not b1) (Not b2))"

lemma while_empties: "((WHILE b DO c), s) \<Rightarrow> t \<Longrightarrow> (bval b t = False)"
  apply (induction "While b c" s t rule:big_step_induct)
   apply auto
  done


lemma "WHILE Or b1 b2 DO c \<sim> WHILE Or b1 b2 DO c;; WHILE b1 DO d"
  apply auto[1]
  apply (rule Seq) apply auto
  apply (drule while_empties) apply auto[1]
  apply (frule while_empties) apply auto
  done


(* 7.6 *)

abbreviation
  DoWhile :: "bexp \<Rightarrow> com \<Rightarrow> com" where
  "DoWhile b c \<equiv> (c ;; While b c)"

fun dewhile :: "com \<Rightarrow> com" where
"dewhile SKIP = SKIP" |
"dewhile (Assign v a) = Assign v a" |
"dewhile (Seq c1 c2) = Seq (dewhile c1) (dewhile c2) " |
"dewhile (If b c1 c2) = If b (dewhile c1) (dewhile c2)" |
"dewhile (While b c) = If b (DoWhile b (dewhile c)) SKIP"

lemma dewhile_while : "dewhile (While b c) = If b (DoWhile b (dewhile c)) SKIP"
  apply auto
  done

lemma if_cong : "c1 \<sim> c1' \<Longrightarrow> c2 \<sim> c2' \<Longrightarrow> IF b THEN c1 ELSE c2 \<sim> IF b THEN c1' ELSE c2'"
  apply auto
  done

lemma while_cong : "c \<sim> c'  \<Longrightarrow> While b c \<sim> While b c'"
  apply (rule sim_while_cong; auto)
  done

lemma dewhile_if: "(dewhile (If b c1 c2) \<sim> If b (dewhile c1) (dewhile c2))"
  apply (case_tac "dewhile c1"; case_tac "dewhile c2"; auto)
  done

lemma dewhile_cong_while: "(dewhile (While b c) \<sim> While b (dewhile c))"
  apply auto
  done

lemma dowhile_while_aux: "IF b THEN DoWhile b c ELSE SKIP \<sim> WHILE b DO c"
  apply auto
  done

lemma dowhile_while: "c \<sim> c' \<Longrightarrow> IF b THEN DoWhile b c ELSE SKIP \<sim> WHILE b DO c'"
  apply (auto simp:dowhile_while_aux while_cong)
  done

lemma "dewhile c \<sim> c"
  apply (induction c rule:dewhile.induct)
  apply auto[3]
  apply (rule sim_trans [OF dewhile_if])
  apply auto[1]
  apply (subst dewhile_while)
  apply simp
  apply (rule dowhile_while)
  apply simp
  done











  
  
  
  






  



  

end