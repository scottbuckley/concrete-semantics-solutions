theory "Ex5-2" imports Main begin
  
(* Exercise 5.2. Give a readable, structured proof of the following lemma:

lemma "\<exists> ys zs. xs = ys @ zs \<and>
  (length ys = length zs \<or> length ys = length zs + 1)"

Hint: There are predefined functions take :: nat \<Rightarrow> 'a list \<Rightarrow> 'a list and drop
:: nat \<Rightarrow> 'a list \<Rightarrow> 'a list such that take k [x\<^sub>1,...] = [x\<^sub>1,..., x\<^sub>k] and drop k
[x\<^sub>1, ...] = [x\<^sub>k\<^sub>+\<^sub>1, ...]. Let sledgehammer find and apply the relevant take and
drop lemmas for you. *)

lemma "\<exists> ys zs. xs = ys @ zs \<and>
  (length ys = length zs \<or> length ys = length zs + 1)"
proof -
  let ?ys = "take (length xs - length xs div 2) xs"
  let ?zs = "drop (length xs - length xs div 2) xs"
  have "xs = ?ys @ ?zs" by simp
  moreover have "(length ?ys = length ?zs) \<or>
                      (length ?ys = length ?zs + 1)" by auto
  ultimately show ?thesis
    by metis this
qed

end