theory "Ex3-7" imports "given/BExp" begin

(* Exercise 3.7. Define functions Eq, Le :: aexp \<Rightarrow> aexp \<Rightarrow> bexp and prove
bval (Eq a1 a2) s = (aval a1 s = aval a2 s) and bval (Le a1 a2) s =
(aval a1 s 6 aval a2 s). *)

fun Eq :: "aexp \<Rightarrow> aexp \<Rightarrow> bexp" where
"Eq (N x) (N y) = Bc (x=y)" |
"Eq l r = And (Not (Less l r)) (Not (Less r l))"

fun Le :: "aexp \<Rightarrow> aexp \<Rightarrow> bexp" where
"Le (N x) (N y) = (if (x=y) then (Bc True) else (Less (N x) (N y)) )" |
"Le l r = Not (Less r l)"

theorem "bval (Eq a1 a2) s = (aval a1 s = aval a2 s)"
  apply (induction a1 a2 rule:Eq.induct)
  apply auto
  done

theorem "bval (Le a1 a2) s = (aval a1 s \<le> aval a2 s)"
  apply (induction a1 a2 rule:Le.induct)
  apply auto
  done

end