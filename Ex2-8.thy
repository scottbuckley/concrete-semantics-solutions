theory "Ex2-8" imports Main begin

(* Exercise 2.8. Define a function intersperse :: 'a \<Rightarrow> 'a list \<Rightarrow> 'a list such
that intersperse a [x 1, ..., x n] = [x 1, a, x 2, a, ..., a, x n]. Now prove that
map f (intersperse a xs) = intersperse (f a) (map f xs). *)

fun intersperse :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"intersperse a (h1 # h2 # r) = h1 # a # (intersperse a (h2 # r))" |
"intersperse _ l = l"

lemma "map f (intersperse a xs) = intersperse (f a) (map f xs)"
  apply (induction xs)
  apply (auto)
  apply (case_tac xs)
  apply (auto)
  done

end