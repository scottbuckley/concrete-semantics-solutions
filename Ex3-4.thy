theory "Ex3-4" imports Main begin

(* Exercise 3.4. Take a copy of theory AExp and modify it as follows. Extend
type aexp with a binary constructor Times that represents multiplication.
Modify the definition of the functions aval and asimp accordingly. You can
remove asimp_const. Function asimp should eliminate 0 and 1 from multiplications as well as evaluate constant subterms. Update all proofs concerned. *)

type_synonym vname = string
type_synonym val = int
type_synonym state = "vname \<Rightarrow> val"

datatype aexp = N int | V vname | Plus aexp aexp | Times aexp aexp

fun aval :: "aexp \<Rightarrow> state \<Rightarrow> val" where
"aval (N n) s = n" |
"aval (V x) s = s x" |
"aval (Plus a\<^sub>1 a\<^sub>2) s = aval a\<^sub>1 s + aval a\<^sub>2 s" |
"aval (Times a\<^sub>1 a\<^sub>2) s = aval a\<^sub>1 s * aval a\<^sub>2 s"

fun plus :: "aexp \<Rightarrow> aexp \<Rightarrow> aexp" where
"plus (N i\<^sub>1) (N i\<^sub>2) = N(i\<^sub>1+i\<^sub>2)" |
"plus (N i) a = (if i=0 then a else Plus (N i) a)" |
"plus a (N i) = (if i=0 then a else Plus a (N i))" |
"plus a\<^sub>1 a\<^sub>2 = Plus a\<^sub>1 a\<^sub>2"

fun times :: "aexp \<Rightarrow> aexp \<Rightarrow> aexp" where
"times (N x) (N y) = N (x*y)" |
"times (N x) r = (if (x=0) then (N 0) else (if (x=1) then r else (Times (N x) r)))" |
"times l (N x) = (if (x=0) then (N 0) else (if (x=1) then l else (Times l (N x))))" |
"times l r = Times l r"

lemma aval_plus[simp]: "aval (plus a1 a2) s = aval a1 s + aval a2 s"
  apply(induction a1 a2 rule: plus.induct)
  apply simp_all (* just for a change from auto *)
  done

lemma aval_times[simp]: "aval (times a1 a2) s = aval a1 s * aval a2 s"
  apply(induction a1 a2 rule: times.induct)
  apply simp_all (* just for a change from auto *)
  done

fun asimp :: "aexp \<Rightarrow> aexp" where
"asimp (N n) = N n" |
"asimp (V x) = V x" |
"asimp (Plus a\<^sub>1 a\<^sub>2) = plus (asimp a\<^sub>1) (asimp a\<^sub>2)" |
"asimp (Times a\<^sub>1 a\<^sub>2) = times (asimp a\<^sub>1) (asimp a\<^sub>2)"

theorem "aval (asimp a) s = aval a s"
  apply(induction a)
  apply simp_all
  done

end