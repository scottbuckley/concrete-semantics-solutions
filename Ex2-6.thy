theory "Ex2-6" imports Main begin

(* Exercise 2.6. Starting from the type 'a tree defined in the text, define a
function contents :: 'a tree \<Rightarrow> 'a list that collects all values in a tree in a list,
in any order, without removing dulicates. Then define a function sum_tree
:: nat tree \<Rightarrow> nat that sums up all values in a tree of natural numbers and
prove sum_tree t = sum_list (contents t) (where sum_list is predefined). *)

datatype 'a tree =
Tip |
Node "'a tree" 'a "'a tree"

fun contents :: "'a tree \<Rightarrow> 'a list" where
"contents Tip = []" |
"contents (Node a b c) = b # ((contents a) @ (contents c))"

fun sum_tree :: "nat tree \<Rightarrow> nat" where
"sum_tree Tip = 0" |
"sum_tree (Node a b c) = b + (sum_tree a + sum_tree c)"

lemma contents_sum: "sum_tree t = sum_list (contents t)"
  apply (induction t)
  apply (auto)
  done

end