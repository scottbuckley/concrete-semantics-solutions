theory "Ex2-4" imports Main begin

(* Exercise 2.4. Define a recursive function snoc :: 'a list \<Rightarrow> 'a \<Rightarrow> 'a list
that appends an element to the end of a list. With the help of snoc define
a recursive function reverse :: 'a list \<Rightarrow> 'a list that reverses a list. Prove
reverse (reverse xs) = xs. *)

fun snoc :: "'a list \<Rightarrow> 'a \<Rightarrow> 'a list" where
"snoc [] a = [a]" |
"snoc (h # r) a = h # (snoc r a)"

fun reverse :: "'a list \<Rightarrow> 'a list" where
"reverse [] = []" |
"reverse (h # r) = snoc (reverse r) h"

lemma reverse_snoc [simp]: "reverse (snoc xs a) = a # (reverse xs)"
  apply (induction xs)
  apply (auto)
  done

lemma reverse_reverse: "reverse (reverse xs) = xs"
  apply (induction xs)
  apply (auto)
  done

end