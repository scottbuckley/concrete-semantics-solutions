theory "Ex3-10" imports "given/BExp" begin

(* Exercise 3.10. A stack underflow occurs when executing an ADD instruction
on a stack of size less than 2. In our semantics a term exec1 ADD s stk where
length stk < 2 is simply some unspecified value, not an error or exception — 
HOL does not have those concepts. Modify theory ASM such that
stack underflow is modelled by None and normal execution by Some, i.e.,
the execution functions have return type stack option. Modify all theorems
and proofs accordingly. *)

datatype instr = LOADI val | LOAD vname | ADD

type_synonym stack = "val list"

fun exec1 :: "instr \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack option" where
"exec1 (LOADI n) _ stk  =  Some (n # stk)" |
"exec1 (LOAD x) s stk  =  Some (s(x) # stk)" |
"exec1 ADD _ (j # i # stk)  =  Some ((i + j) # stk)" |
"exec1 ADD _ _ = None"

fun exec :: "instr list \<Rightarrow> state \<Rightarrow> stack option \<Rightarrow> stack option" where
"exec _ _ None = None" |
"exec [] _ stko = stko" |
"exec (i#is) s (Some stk) = exec is s (exec1 i s stk)"

lemma exec_append[simp]: "exec (is1@is2) s stko = exec is2 s (exec is1 s stko)"
  apply(induction is1 arbitrary: is2 stko)
  apply (case_tac stko) apply auto
  apply (case_tac stko) apply auto
  done

fun comp :: "aexp \<Rightarrow> instr list" where
"comp (N n) = [LOADI n]" |
"comp (V x) = [LOAD x]" |
"comp (Plus e\<^sub>1 e\<^sub>2) = comp e\<^sub>1 @ comp e\<^sub>2 @ [ADD]"

theorem exec_comp: "exec (comp a) s (Some stk) = Some ((aval a s) # stk)"
  apply(induction a arbitrary: stk)
  apply (auto)
  done

end