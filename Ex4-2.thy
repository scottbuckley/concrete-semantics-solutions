theory "Ex4-2" imports Main begin
  
(* Exercise 4.2. Formalize the following definition of palindromes
 - The empty list and a singleton list are palindromes.
 - If xs is a palindrome, so is a # xs @ [a].
as an inductive predicate palindrome :: 'a list \<Rightarrow> bool and prove that rev xs
= xs if xs is a palindrome. *)

inductive palindrome :: "'a list \<Rightarrow> bool" where
empty:  "palindrome []" |
single: "palindrome [a]" |
outer:  "palindrome as \<Longrightarrow> palindrome (a # as @ [a])"

theorem "palindrome xs \<Longrightarrow> rev xs = xs"
  apply (induction xs rule:palindrome.induct)
  apply auto
  done

end