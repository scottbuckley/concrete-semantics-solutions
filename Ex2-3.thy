theory "Ex2-3" imports Main begin

(* Exercise 2.3. Define a function count :: 'a \<Rightarrow> 'a list \<Rightarrow> nat that counts the
number of occurrences of an element in a list. Prove count x xs \<le> length xs *)

fun count :: "'a \<Rightarrow> 'a list \<Rightarrow> nat" where
"count _ [] = 0" |
"count a (b # hs) = (if (a=b) then (Suc(count a hs)) else (count a hs))"

lemma count_less: "count x xs \<le> length xs"
  apply (induction xs)
  apply (auto)
  done

end