theory "Ex4-3" imports "given/Star" begin
  
(* Exercise 4.3. We could also have defined star as follows:

inductive star' :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" for r where
refl': "star' r x x" |
step': "star' r x y \<Longrightarrow> r y z \<Longrightarrow> star' r x z"

The single r step is performed after rather than before the star'
steps. Prove star' r x y \<Longrightarrow> star r x y and star r x y \<Longrightarrow> star' r x y.
You may need lemmas. Note that rule induction fails if the
assumption about the inductive predicate is not the first assumption. *)

inductive star' :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" for r where
refl': "star' r x x" |
step': "star' r x y \<Longrightarrow> r y z \<Longrightarrow> star' r x z"

lemma star'_first_aux: "star' r y z \<Longrightarrow> r x y \<Longrightarrow> star' r x z"
  apply (induction rule:star'.induct)
  apply (auto intro:star'.intros)
  done

lemma star'_first: "r x y \<Longrightarrow> star' r y z \<Longrightarrow> star' r x z"
  apply (metis star'_first_aux)
  done

lemma star_last: "star r x y \<Longrightarrow> r y z \<Longrightarrow> star r x z"
  apply (induction rule:star.induct)
  apply (auto simp add:star.step)
  done

lemma star_star': "star r x y \<Longrightarrow> star' r x y"
  apply (induction rule:star.induct)
  apply (rule refl')
  apply (auto simp add:star'_first)
  done

lemma star'_star: "star' r x y \<Longrightarrow> star r x y"
  apply (induction rule:star'.induct)
  apply (rule star.refl)
  apply (auto simp add:star_last)
  done

end