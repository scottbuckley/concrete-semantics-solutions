theory "Ex3-11" imports "given/BExp" begin

(* Exercise 3.11. This exercise is about a register machine and compiler for
aexp. The machine instructions are

datatype instr = LDI int reg | LD vname reg | ADD reg reg

where type reg is a synonym for nat. Instruction LDI i r loads i into register
r, LD x r loads the value of x into register r, and ADD r 1 r 2 adds register
r 2 to register r 1.

Define the execution of an instruction given a state and a register state
(= function from registers to integers); the result is the new register state:

fun exec1 :: instr \<Rightarrow> state \<Rightarrow> (reg \<Rightarrow> int) \<Rightarrow> reg \<Rightarrow> int

Define the execution exec of a list of instructions as for the stack machine.

The compiler takes an arithmetic expression a and a register r and produces a
list of instructions whose execution places the value of a into r. The
registers > r should be used in a stack-like fashion for intermediate results,
the ones < r should be left alone. Define the compiler and prove it correct:
exec (comp a r ) s rs r = aval a s. *)

type_synonym reg = nat
datatype instr = LDI int reg | LD vname reg | ADD reg reg

fun exec1 :: "instr \<Rightarrow> state \<Rightarrow> (reg \<Rightarrow> int) \<Rightarrow> reg \<Rightarrow> int" where
"exec1 (LDI i r1)  _ rs = rs(r1 := i) " |
"exec1 (LD n r1)   s rs = rs(r1 := s n)" |
"exec1 (ADD r1 r2) _ rs = rs(r1 := (rs r1)+(rs r2))"

fun exec :: "instr list \<Rightarrow> state \<Rightarrow> (reg \<Rightarrow> int) \<Rightarrow> reg \<Rightarrow> int" where
"exec [] _ rs = rs" |
"exec (i # is) s rs = exec is s (exec1 i s rs)"

fun comp :: "aexp \<Rightarrow> reg \<Rightarrow> instr list" where
"comp (N n) r = [LDI n r]" |
"comp (V x) r = [LD x r]" |
"comp (Plus a1 a2) r = comp a1 r @ comp a2 (r+1) @ [ADD r (r+1)]"

lemma exec_append[simp]: "exec (is1@is2) s rs = exec is2 s (exec is1 s rs)"
  apply(induction is1 s rs rule:exec.induct)
  apply auto
  done

lemma exec_before: "r1 > r2 \<Longrightarrow>(exec (comp a r1) s rs) r2 = rs r2"
  apply (induction a r1 arbitrary:r2 rs s rule:comp.induct)
  apply auto
  done
  
theorem "exec (comp a r) s rs r = aval a s"
  apply (induction a r arbitrary:r rs rule:comp.induct)
  apply (auto simp:exec_before)
  done

end