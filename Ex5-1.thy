theory "Ex5-1" imports Main begin
  
(* Exercise 5.1. Give a readable, structured proof of the following lemma:
lemma assumes T: "\<forall> x y. T x y \<or> T y x"
and A: "\<forall> x y. A x y \<and> A y x \<longrightarrow> x = y"
and TA: "\<forall> x y. T x y \<longrightarrow> A x y" and Axy: "A x y"
shows "T x y" *)

lemma assumes T: "\<forall> x y. T x y \<or> T y x"
and A: "\<forall> x y. A x y \<and> A y x \<longrightarrow> x = y"
and TA: "\<forall> x y. T x y \<longrightarrow> A x y" and Axy: "A x y"
shows "T x y"
proof -
  have "T x y \<or> T y x" using T by auto
  then show "T x y"
  proof
    assume "T x y"
    then show "T x y" by assumption
  next
    assume Tyx: "T y x"
    then have Ayx:"A y x" using TA by auto
    moreover have "x = y" using A Axy Ayx by auto
    ultimately show "T x y" using Tyx by auto
  qed
qed

end