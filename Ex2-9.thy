theory "Ex2-9" imports "Ex2-2" begin

(* Exercise 2.9. Write a tail-recursive variant of the add function on nat:
itadd. Tail-recursive means that in the recursive case, itadd needs to call
itself directly: itadd (Suc m) n = itadd . . .. Prove itadd m n = add m n. *)

(* NB: Requires definition of "add" from Ex2.2 *)

fun itadd :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
"itadd 0 n = n" |
"itadd (Suc m) n = itadd m (Suc n)"

lemma "itadd m n = add m n"
  apply (induction m arbitrary: n)
  apply auto
  done

end