theory CSE5 imports CSE3a begin



(* 5.1 *)
lemma assumes T: "\<forall> x y. T x y \<or> T y x"
and A: "\<forall> x y. A x y \<and> A y x \<longrightarrow> x = y"
and TA: "\<forall> x y. T x y \<longrightarrow> A x y" and Axy: "A x y"
shows "T x y"
proof -
  have Tor: "T x y \<or> T y x" using T by auto
  then show "T x y"
  proof
    assume Txy: "T x y"
    show "T x y" using Txy by assumption
  next
    assume Tyx: "T y x"
    have Ayx:"A y x" using TA Tyx by auto
    have Axy:"A x y" using Axy by auto
    have Esm: "x = y" using A Axy Ayx by auto
    show "T x y" using Esm Tyx by auto
  qed
qed


fun abl :: "'a list \<Rightarrow> 'a list" where
"abl [] = []" |
"abl [a] = []" |
"abl (h#r) = h#(abl r)"

fun last :: "'a list \<Rightarrow> 'a list" where
"last [] = []" |
"last [r] = [r]" | 
"last (h#r) = last r"

lemma abl_last: "abl x @ last x = x"
  apply (induction x rule:abl.induct)
    apply auto
  done


lemma last_length: "length l = Suc x \<Longrightarrow> length (last l) = 1"
  apply (induction l arbitrary:x rule:last.induct)
    apply auto
  done

lemma abl_length: "length l = Suc x \<Longrightarrow> length (abl l) = x"
  apply (induction l arbitrary:x rule:last.induct)
    apply auto
  done
  

(* 5.2 *)
lemma "\<exists> ys zs. xs = ys @ zs \<and>
  (length ys = length zs \<or> length ys = length zs + 1)"
  apply (induction xs)
   apply simp
  apply auto
  apply (rule_tac x="a#ys" in exI)
   apply (rule_tac x="zs" in exI)
   apply auto
  apply (rule_tac x="a#(abl ys)" in exI)
  apply (rule_tac x="(last ys)@zs" in exI)
  apply (subst append_Cons)
  apply (subst append_assoc[symmetric])
  apply (subst abl_last) apply simp
  apply (subst last_length) apply simp
  apply (subst last_length) apply simp
  apply (subst abl_length) apply simp
  apply auto
  done
 
  
  
  
  









  
  







end