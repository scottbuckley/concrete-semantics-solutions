theory "Ex5-7" imports "Ex4-5" "Lib.Lib" begin
  
(* Exercise 5.7. Extend Exercise 4.5 with a function that checks if some
alpha list is a balanced string of parentheses. More precisely, define a recursive
function balanced :: nat \<Rightarrow> alpha list \<Rightarrow> bool such that balanced n w is true
iff (informally) S (a\<^sup>n @ w). Formally, prove that balanced n w = S (replicate
n a @ w) where replicate :: nat \<Rightarrow> 'a \<Rightarrow> 'a list is predefined and replicate
n x yields the list [x , ..., x ] of length n. *)

(* datatype alpha = aA | aB | aC *)

(* inductive S :: "alpha list \<Rightarrow> bool" where *)
(* Sempty: "S []" | *)
(* Saround: "S mid \<Longrightarrow> S (aA # mid @ [aB])" | *)
(* Sconcat: "S l \<Longrightarrow> S r \<Longrightarrow> S (l @ r)" *)

(* inductive T :: "alpha list \<Rightarrow> bool" where *)
(* Tempty: "T []" | *)
(* Tab:    "T l \<Longrightarrow> T r \<Longrightarrow> T (l @ aA # r @ [aB])" *)


fun bal :: "nat \<Rightarrow> alpha list \<Rightarrow> bool" where
"bal 0 [] = True" |
"bal n (aA#r) = bal (Suc n) r" |
"bal (Suc n) (aB#r) = bal n r" |
"bal _ _ = False"

lemma bal_concat: "bal n l \<Longrightarrow> bal 0 r \<Longrightarrow> bal n (l@r)"
  apply (induction arbitrary:r rule:bal.induct)
  apply auto
  done

lemma bal_outside: "bal n m \<Longrightarrow> bal n (aA # m @ [aB])"
  apply (induction n m rule:bal.induct)
  apply auto
  done

theorem S_bal_0: "S l \<Longrightarrow> bal 0 l"
  apply (induction l rule:S.induct)
  using bal_outside bal_concat apply auto
  done

(* true but i think not needed
lemma bal_endB : "bal n l \<Longrightarrow> bal (Suc n) (l@[aB])" using bal_outside by auto

lemma S_a : "S (aA # r) \<Longrightarrow> bal 1 r"
  apply (induction "aA#r" arbitrary:r rule:S.induct)
  using S_bal bal_endB apply auto[1]
  apply (metis S_bal append_eq_Cons_conv bal_concat)
  done
*)

theorem S_bal_n : "S (replicate n aA @ l) \<Longrightarrow> bal n l"
  apply (induction n arbitrary: l)
  using S_bal_0 apply auto[1]
  apply (metis append_Cons bal.simps(2) replicate_Suc replicate_app_Cons_same)
  done

(*
theorem bal_S_0 : "bal 0 l \<Longrightarrow> S l"
  apply (induction rule:bal.induct)
  using S.intros apply auto[1]
  apply clarsimp
  sledgehammer
*)

lemma S_rep_ab : " S(replicate n aA @ r) \<Longrightarrow> S(replicate n aA @ aA # aB # r)"
  apply (induction n arbitrary:r)
  using S.intros apply fastforce
  

theorem bal_S_n : "bal n l \<Longrightarrow> S (replicate n aA @ l)"
  apply (induction n l rule:bal.induct)
  using S.intros apply auto[1]
  apply (simp add: replicate_app_Cons_same)
  sledgehammer
  
  












fun balanced :: "nat \<Rightarrow> alpha list \<Rightarrow> bool" where
"balanced n       (aA#r) = balanced (Suc n) r" |
"balanced (Suc n) (aB#r) = balanced n r" |
"balanced 0       []     = True" |
"balanced 0       (h#r)  = False" |
"balanced (Suc n) []     = False"


lemma outside_append: "h # mid @ [t] = l @ r \<Longrightarrow> l=[] \<or> r=[] \<or> (\<exists> l' r'. l=h#l' \<and> r=r'@[t])"
  by (metis Cons_eq_append_conv  append_eq_Cons_conv append_eq_append_conv2 append_is_Nil_conv)

lemma takedrop: "l = (take n l) @ (drop n l)"
  by simp

lemma lessgreater : "(a::nat) \<noteq> b \<Longrightarrow> a<b \<or> b<a"
  by (simp add: nat_neq_iff)

lemma append_eq_takedrop : "L@R = P@Q \<Longrightarrow> P = take (length P) (L@R) \<and> Q = drop (length P) (L@R)"
  by auto

lemma append_eq : "L@R = P@Q \<Longrightarrow> (L=P \<and> R=Q)
                \<or> (\<exists> n. P=L@(take n R) \<and> Q=(drop n R))
                \<or> (\<exists> n. P=(take n L) \<and> Q=(drop n L)@R)"
  apply (drule append_eq_takedrop)
  apply (erule conjE)
  apply (cases "length L = length P")
  apply fastforce
  apply (drule lessgreater)
  apply auto
  done
  
lemma not_S_b : "\<not> S (aB # r)"
  apply clarsimp
  apply (induction "aB#r" arbitrary:r rule:S.induct)
  apply (meson append_eq_Cons_conv)
  done


lemma S_middle: "S (l @ r) \<Longrightarrow> S (l @ [aA, aB] @ r)"
  apply (induction "l@r" arbitrary:l r rule:S.induct)
  using Saround Sempty apply fastforce
  apply (frule outside_append)
  apply auto[1]
  apply (metis S.simps append.left_neutral append_Cons)[1]
  apply (metis (mono_tags) S.simps append.assoc append_Cons append_Nil)[1]
  apply (metis Cons_eq_appendI S.simps append_eq_appendI)[1]
  apply (frule append_eq)
  apply (erule disjE)
  using Sconcat apply fastforce
  apply (erule disjE)
  using Sconcat apply fastforce
  apply (metis S.simps append.assoc takedrop)
  done

(*
lemma S_middle': "S (l @ [aA, aB] @ r) \<Longrightarrow> S (l @ r)"
  apply (induction "l @ [aA, aB] @ r" arbitrary:l r rule:S.induct)
  using Saround Sempty apply fastforce
  
   apply (frule outside_append)
  sledgehammer
  apply auto[1]
  apply (metis S.simps append.left_neutral append_Cons)[1]
  apply (metis (mono_tags) S.simps append.assoc append_Cons append_Nil)[1]
  apply (metis Cons_eq_appendI S.simps append_eq_appendI)[1]
  apply (frule append_eq)
  apply (erule disjE)
  using Sconcat apply fastforce
  apply (erule disjE)
  using Sconcat apply fastforce
  apply (metis S.simps append.assoc takedrop)
  done *)


lemma replicate_Suc_end : "replicate (Suc n) j = replicate n j @ [j]"
  by (simp add: replicate_append_same)

lemma list_rewrite_temp : "(a @ [b]) @ c # d = a @ ([b, c]) @ d"
  by auto

theorem "balanced n w \<Longrightarrow> S (replicate n aA @ w)"
  apply (induction n w rule:balanced.induct)
  apply (simp add: replicate_app_Cons_same)
  defer
  apply (simp add: Sempty)
  apply (simp add: not_S_b)
  apply auto[1]
  apply (metis S_middle balanced.simps(2) list_rewrite_temp replicate_Suc replicate_append_same)
  done

theorem "S (replicate n aA @ w) \<Longrightarrow> balanced n w"
  apply (induction "replicate n aA @ w" arbitrary:n w rule:S.induct)
  apply auto[1]
  sledgehammer
  apply (simp add: replicate_app_Cons_same)
  defer
  apply (simp add: Sempty)
  apply (simp add: not_S_b)
  apply auto[1]
  apply (metis S_middle balanced.simps(2) list_rewrite_temp replicate_Suc replicate_append_same)
  done


end