theory "Ex4-1" imports Main begin
  
(* Exercise 4.1. Start from the data type of binary trees defined earlier:

datatype 'a tree = Tip | Node " 'a tree" 'a " 'a tree"

Define a function set :: 'a tree \<Rightarrow> 'a set that returns the elements in a tree
and a function ord :: int tree \<Rightarrow> bool that tests if an int tree is ordered.

Define a function ins that inserts an element into an ordered int tree
while maintaining the order of the tree. If the element is already in the tree,
the same tree should be returned. Prove correctness of ins: set (ins x t) =
{x } \<union> set t and ord t \<Longrightarrow> ord (ins i t). *)

datatype 'a tree = Tip | Node "'a tree" 'a " 'a tree"

fun set :: "'a tree \<Rightarrow> 'a set" where
"set Tip = {}" |
"set (Node t1 v t2) = (set t1 \<union> {v} \<union> set t2)"

fun ord :: "int tree \<Rightarrow> bool" where
"ord Tip = True" |
"ord (Node l n r) = ((ord l) \<and> (ord r) \<and> (\<forall> v \<in> set l. v \<le> n) \<and> (\<forall> v \<in> set r. v > n))"

fun ins :: "int \<Rightarrow> int tree \<Rightarrow> int tree" where
"ins v Tip = Node Tip v Tip" |
"ins v (Node l n r) = (
     if (v=n) then (Node l n r) else (
     if (v<n) then (Node (ins v l) n r) else (
     Node l n (ins v r)
)))"

theorem [simp]: "set (ins x t) = {x} \<union> set t"
  apply (induction t rule:ins.induct)
  apply auto
  done

theorem "ord t \<Longrightarrow> ord (ins i t)"
  apply (induction t rule:ins.induct)
  apply auto
  done

end