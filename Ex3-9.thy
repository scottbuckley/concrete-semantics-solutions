theory "Ex3-9" imports "given/BExp" begin

(* Exercise 3.9. Define a new type of purely boolean expressions

datatype pbexp =VAR vname | NOT pbexp | AND pbexp pbexp | OR pbexp pbexp

where variables range over values of type bool:

fun pbval :: "pbexp \<Rightarrow> (vname \<Rightarrow> bool) \<Rightarrow> bool" where
"pbval (VAR x ) s = s x" |
"pbval (NOT b) s = (\<not> pbval b s)" |
"pbval (AND b1 b2) s = (pbval b1 s \<and> pbval b2 s)" |
"pbval (OR b1 b2) s = (pbval b1 s \<or> pbval b2 s)"

Define a function is_nnf :: pbexp \<Rightarrow> bool that checks whether a boolean
expression is in NNF (negation normal form), i.e., if NOT is only applied
directly to VARs. Also define a function nnf :: pbexp \<Rightarrow> pbexp that converts
a pbexp into NNF by pushing NOT inwards as much as possible. Prove that
nnf preserves the value (pbval (nnf b) s = pbval b s) and returns an NNF
(is_nnf (nnf b)).

An expression is in DNF (disjunctive normal form) if it is in NNF and if
no OR occurs below an AND. Define a corresponding test is_dnf :: pbexp \<Rightarrow>
bool. An NNF can be converted into a DNF in a bottom-up manner.
The critical case is the conversion of AND b1 b2. Having converted b1 and b2, apply
distributivity of AND over OR. Define a conversion function dnf_of_nnf ::
pbexp \<Rightarrow> pbexp from NNF to DNF. Prove that your function preserves the
value (pbval (dnf_of_nnf b) s = pbval b s) and converts an NNF into a
DNF (is_nnf b =\<Rightarrow> is_dnf (dnf_of_nnf b)). *)


datatype pbexp = VAR vname | NOT pbexp | AND pbexp pbexp | OR pbexp pbexp

fun pbval :: "pbexp \<Rightarrow> (vname \<Rightarrow> bool) \<Rightarrow> bool" where
"pbval (VAR x ) s = s x" |
"pbval (NOT b) s = (\<not> pbval b s)" |
"pbval (AND b1 b2) s = (pbval b1 s \<and> pbval b2 s)" |
"pbval (OR b1 b2) s = (pbval b1 s \<or> pbval b2 s)"

fun is_nnf :: "pbexp \<Rightarrow> bool" where
"is_nnf (VAR _) = True" |
"is_nnf (AND a b) = (is_nnf a \<and> is_nnf b)" |
"is_nnf (OR a b)  = (is_nnf a \<and> is_nnf b)" |
"is_nnf (NOT (VAR _)) = True" |
"is_nnf _ = False"

fun nnf :: "pbexp \<Rightarrow> pbexp" where
"nnf (VAR x) = VAR x" |
"nnf (AND b1 b2) = AND (nnf b1) (nnf b2)" |
"nnf (OR b1 b2)  = OR  (nnf b1) (nnf b2)" |
"nnf (NOT (VAR x)) = NOT (VAR x)" |
"nnf (NOT (NOT b)) = nnf b" |
"nnf (NOT (AND b1 b2)) = OR (nnf (NOT b1)) (nnf (NOT b2))" |
"nnf (NOT (OR b1 b2)) = AND (nnf (NOT b1)) (nnf (NOT b2))"


theorem nnf_preserves : "pbval (nnf b) s = pbval b s"
  apply (induction b rule:nnf.induct)
  apply auto
  done

theorem nnf_isnnf : "is_nnf (nnf b)"
  apply (induction b rule:nnf.induct)
  apply auto
  done


fun is_dnf :: "pbexp \<Rightarrow> bool" where
"is_dnf (VAR _) = True" |
"is_dnf (AND b1 b2) = (case (b1, b2) of
  ((OR _ _), _) \<Rightarrow> False |
  (_, (OR _ _)) \<Rightarrow> False |
  (b1, b2)      \<Rightarrow> (is_dnf b1 \<and> is_dnf b2)
)" |
"is_dnf (OR a b)  = (is_dnf a \<and> is_dnf b)" |
"is_dnf (NOT (VAR _)) = True" |
"is_dnf _ = False"

(* we assume the inputs will be DNF *)
fun distrib_ors :: "pbexp \<Rightarrow> pbexp \<Rightarrow> pbexp" where
"distrib_ors (OR b1 b2) (OR b3 b4) = OR (OR (distrib_ors b1 b3) (distrib_ors b1 b4)) (OR (distrib_ors b2 b3) (distrib_ors b2 b4))" |
"distrib_ors b1 (OR b2 b3) = OR (distrib_ors b1 b2) (distrib_ors b1 b3)" |
"distrib_ors (OR b1 b2) b3 = OR (distrib_ors b1 b3) (distrib_ors b2 b3)" |
"distrib_ors b1 b2 = AND b1 b2"

fun dnf_of_nnf :: "pbexp \<Rightarrow> pbexp" where
"dnf_of_nnf (AND b1 b2) = distrib_ors (dnf_of_nnf b1) (dnf_of_nnf b2)" |
"dnf_of_nnf (OR b1 b2) = OR (dnf_of_nnf b1) (dnf_of_nnf b2)" |
"dnf_of_nnf (NOT b) = NOT b" |
"dnf_of_nnf (VAR n) = VAR n"

lemma dnf_over_distrib [elim]: "is_dnf b1 \<Longrightarrow> is_dnf b2 \<Longrightarrow> is_dnf (distrib_ors b1 b2)"
  apply (induction b1 b2 rule:distrib_ors.induct)
  apply auto
  done

theorem dnf_of_nnf_converts: "is_nnf b \<Longrightarrow> is_dnf (dnf_of_nnf b)"
  apply (induction b rule:dnf_of_nnf.induct)
  apply auto
  apply (case_tac b; simp)
  done

lemma distrib_ors_preserves [simp]: "pbval (distrib_ors b1 b2) s = pbval (AND b1 b2) s"
  apply (induction b1 b2 rule:distrib_ors.induct)
  apply auto
  done

theorem dnf_of_nnf_preserves: "pbval (dnf_of_nnf b) s = pbval b s"
  apply (induction b rule:dnf_of_nnf.induct)
  apply auto
  done

end