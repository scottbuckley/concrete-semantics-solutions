theory "Ex5-6" imports Main begin
  
(* Exercise 5.6. Define a recursive function elems :: 'a list \<Rightarrow> 'a set and prove
x \<in> elems xs \<Longrightarrow> \<exists> ys zs. xs = ys @ x # zs \<and> x \<notin> elems ys. *)

fun elems :: "'a list \<Rightarrow> 'a set" where
"elems [] = {}" |
"elems (h # r) = {h} \<union> (elems r)"

lemma "x \<in> elems xs \<Longrightarrow> \<exists> ys zs. xs = ys @ x # zs \<and> x \<notin> elems ys"
proof (induct xs rule: elems.induct[case_names Nil Cons])
  case Nil
  then show ?case by (simp add:emptyE)
next
  case (Cons h r)
  then show ?case
  proof (cases "h = x" "x \<in> elems r" rule: disjE[case_names disj left right])
    case disj
    then show ?case using Cons by auto
  next
    case left
    then show ?thesis by (rule_tac x="[]" in exI; simp)
  next
    case right
    then show ?thesis
    proof (cases "x=h")
      case True
      then show ?thesis by (rule_tac x="[]" in exI; simp)
    next
      case False
      then obtain ys zs where IH:
      "r = ys @ x # zs"
      "x \<notin> elems ys" using Cons by auto
      then show ?thesis using False by (rule_tac x="h#ys" in exI; auto)
    qed
  qed
qed

end