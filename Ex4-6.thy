theory "Ex4-6" imports "given/AExp" begin
  
(* Exercise 4.6. In Section 3.1 we defined a recursive evaluation function aval
:: aexp \<Rightarrow> state \<Rightarrow> val. Define an inductive evaluation predicate aval_rel
:: aexp \<Rightarrow> state \<Rightarrow> val \<Rightarrow> bool and prove that it agrees with the recursive
function: aval_rel a s v \<Longrightarrow> aval a s = v, aval a s = v \<Longrightarrow> aval_rel a s v
and thus aval_rel a s v \<longleftrightarrow> aval a s = v. *)

inductive aval_rel :: "aexp \<Rightarrow> state \<Rightarrow> val \<Rightarrow> bool" where
avalN : "aval_rel (N n) _ n" |
avalV : "aval_rel (V x) s (s x)" |
avalPl: "aval_rel a s av \<Longrightarrow> aval_rel b s bv \<Longrightarrow> aval_rel (Plus a b) s (av+bv)"

theorem avalrel_aval: "aval_rel a s v \<Longrightarrow> aval a s = v"
  apply (induction rule:aval_rel.induct)
  apply auto
  done

theorem aval_avalrel: "aval a s = v \<Longrightarrow> aval_rel a s v"
  apply (induction a arbitrary: s v rule:aexp.induct)
  apply (auto intro:aval_rel.intros)
  done

theorem avalrel__aval: "aval_rel a s v \<longleftrightarrow> aval a s = v"
  apply (rule iffI)
  apply (rule avalrel_aval; simp)
  apply (rule aval_avalrel; simp)
  done

end