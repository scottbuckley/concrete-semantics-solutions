theory "Ex3-2" imports "given/AExp" begin

(* Exercise 3.2. In this exercise we verify constant folding for aexp where we
sum up all constants, even if they are not next to each other. For example, Plus
(N 1) (Plus (V x ) (N 2)) becomes Plus (V x ) (N 3). This goes beyond asimp.
Define a function full_asimp :: aexp \<Rightarrow> aexp that sums up all constants and
prove its correctness: aval (full_asimp a) s = aval a s. *)

fun full_asimp :: "aexp \<Rightarrow> aexp" where
"full_asimp (N n) = N n" |
"full_asimp (V x) = Plus (V x) (N 0)" |
"full_asimp (Plus a1 a2) = (
  case (full_asimp a1, full_asimp a2) of
    (Plus lr (N lx), Plus rl (N rx)) \<Rightarrow> Plus (Plus lr rl) (N (lx + rx)) |
    (N lx, Plus rl (N rx)) \<Rightarrow> Plus rl (N (lx+rx)) |
    (Plus ll (N lx), N rx) \<Rightarrow> Plus ll (N (lx+rx)) |
    (N lx, N rx) \<Rightarrow> N (lx+rx) |
    (l, r) \<Rightarrow> Plus l r
  )"

theorem "aval (full_asimp a) s = aval a s"
  apply (induction a)
  apply (auto split:aexp.split)
  done

end