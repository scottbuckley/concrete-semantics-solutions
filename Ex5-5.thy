theory "Ex5-5" imports "Ex4-4" begin
  
(* Exercise 5.5. Recall predicate star from Section 4.5.2 and iter
from Exercise 4.4. Prove iter r n x y \<Longrightarrow> star r x y in a structured style;
do not just sledgehammer each case of the required induction. *)

theorem "iter r n x y \<Longrightarrow> star r x y"
proof -
  assume "iter r n x y"
  then show "star r x y"
  proof induction
    case (refli r x)
    then show ?case by auto
  next
    case (stepi r x y n z)
    then show ?case by (auto simp add:star.step)
  qed
qed

end