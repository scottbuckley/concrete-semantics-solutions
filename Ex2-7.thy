theory "Ex2-7" imports Main begin

(* Exercise 2.7. Define a new type 'a tree2 of binary trees where values are
also stored in the leaves of the tree. Also reformulate the mirror function
accordingly. Define two functions pre_order and post_order of type 'a tree2
\<Rightarrow> 'a list that traverse a tree and collect all stored values in the respective
order in a list. Prove pre_order (mirror t) = rev (post_order t). *)

datatype 'a tree2 = 
Tip 'a |
Node "'a tree2" 'a "'a tree2"

fun mirror2 :: "'a tree2 \<Rightarrow> 'a tree2" where
"mirror2 (Tip a) = Tip a" |
"mirror2 (Node a b c) = Node (mirror2 c) b (mirror2 a)"

fun pre_order :: "'a tree2 \<Rightarrow> 'a list" where
"pre_order (Tip a) = [a]" |
"pre_order (Node l c r) = [c] @ (pre_order l @ pre_order r)"

fun post_order :: "'a tree2 \<Rightarrow> 'a list" where
"post_order (Tip a) = [a]" |
"post_order (Node l c r) = (post_order l @ post_order r) @ [c]"

lemma "pre_order (mirror2 t) = rev (post_order t)"
  apply (induction t)
  apply (auto)
  done

end