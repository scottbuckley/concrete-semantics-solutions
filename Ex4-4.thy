theory "Ex4-4" imports "given/Star" begin
  
(* Exercise 4.4. Analogous to star, give an inductive definition of the n-fold
iteration of a relation r : iter r n x y should hold if there are x\<^sub>0, ... , x\<^sub>n such
that x = x\<^sub>0, x\<^sub>n = y and r x\<^sub>i x\<^sub>i\<^sub>+\<^sub>1 for all i < n. Correct and prove the
following claim: star r x y \<Longrightarrow> iter r n x y. *)

inductive iter :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> nat \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
refli: "iter r 0 x x" |
stepi: "r x y \<Longrightarrow> iter r n y z \<Longrightarrow> iter r (Suc n) x z"

lemma star_iter: "star r x y \<Longrightarrow> \<exists> n. iter r n x y"
  apply (induction rule:star.induct)
  apply (rule_tac x=0 in exI)
  apply (rule refli)
  apply clarsimp
  apply (rule_tac x="Suc n" in exI)
  apply (auto intro:stepi)
  done

end