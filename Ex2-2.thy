theory "Ex2-2" imports Main begin

(* Exercise 2.2. Start from the definition of add given above. Prove that add
is associative and commutative. Define a recursive function double :: nat \<Rightarrow>
nat and prove double m = add m m. *)

fun add :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
"add 0 n = n" |
"add (Suc m) n = Suc (add m n)"

lemma add_assoc: "add a (add b c) = add (add a b) c"
  apply (induction a)
  apply (auto)
  done

lemma add_0_right [simp]: "add a 0 = a"
  apply (induction a)
  apply (auto)
  done

lemma add_suc [simp]: " add a (Suc b) = Suc (add a b)"
  apply (induction a)
  apply (auto)
  done

lemma add_comm: "add a b = add b a"
  apply (induction b)
  apply (auto)
  done

fun double :: "nat \<Rightarrow> nat" where
"double 0 = 0"|
"double (Suc n) = Suc (Suc (double n))"

lemma double_add: "double m = m + m"
  apply (induction m)
  apply (auto)
  done

end