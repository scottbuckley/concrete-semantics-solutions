theory "Ex3-1" imports "given/AExp" begin

(* Exercise 3.1. To show that asimp_const really folds all subexpressions of
the form Plus (N i) (N j), define a function optimal :: aexp \<Rightarrow> bool that
checks that its argument does not contain a subexpression of the form Plus
(N i) (N j). Then prove optimal (asimp_const a). *)

fun optimal :: "aexp \<Rightarrow> bool" where
"optimal (Plus (N _) (N _)) = False" |
"optimal (N _) = True" |
"optimal (V _) = True" |
"optimal (Plus a b) = ((optimal a) \<and> (optimal b))"

lemma ex31: "optimal (asimp_const a)"
  apply (induction a)
  apply (auto split: aexp.split)
  done

end