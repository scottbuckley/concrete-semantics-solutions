theory "Ex2-11" imports Main begin

(* Exercise 2.11. Define arithmetic expressions in one variable over integers
(type int) as a data type:

datatype exp = Var | Const int | Add exp exp | Mult exp exp

Define a function eval :: exp \<Rightarrow> int \<Rightarrow> int such that eval e x evaluates e at
the value x.

A polynomial can be represented as a list of coefficients, starting with the
constant. For example, [4, 2, -1, 3] represents the polynomial 4 + 2x - x\<^sup>2 + 3x\<^sup>3.
Define a function evalp :: int list \<Rightarrow> int \<Rightarrow> int that evaluates a polynomial at
the given value. Define a function coeffs :: exp \<Rightarrow> int list that transforms an
expression into a polynomial. This may require auxiliary functions. Prove that
coeffs preserves the value of the expression: evalp (coeffs e) x = eval e x.

Hint: consider the hint in Exercise 2.10. *)

datatype exp = Var | Const int | Add exp exp | Mult exp exp

fun eval :: "exp \<Rightarrow> int \<Rightarrow> int" where
"eval Var x = x" |
"eval (Const a) x = a" |
"eval (Add a b) x = eval a x + eval b x" |
"eval (Mult a b) x = eval a x * eval b x"

fun evalp :: "int list \<Rightarrow> int \<Rightarrow> int" where
"evalp [] _ = 0" |
"evalp (h # r) x = h + x * (evalp r x)"

(* adds two sets of coefficients together *)
fun add_coeffs :: "int list \<Rightarrow> int list \<Rightarrow> int list" where
"add_coeffs [] l = l" |
"add_coeffs l [] = l" |
"add_coeffs (h1 # r1) (h2 # r2) = (h1 + h2) # (add_coeffs r1 r2)"

(* multiplies every coefficient by some integer (maps multiplication) *)
fun scale_coeffs :: "int \<Rightarrow> int list \<Rightarrow> int list" where
"scale_coeffs _ [] = []" |
"scale_coeffs x (h # r) = (h*x) # (scale_coeffs x r)"

(* multiplies two sets of coefficients together *)
fun mult_coeffs :: "int list \<Rightarrow> int list \<Rightarrow> int list" where
"mult_coeffs [] l = []" |
"mult_coeffs (h1 # r1) r2 = add_coeffs (scale_coeffs h1 r2) (0 # (mult_coeffs r1 r2))"

fun coeffs :: "exp \<Rightarrow> int list" where
"coeffs Var = [0, 1]" |
"coeffs (Const x) = [x]" |
"coeffs (Add a b) = add_coeffs (coeffs a) (coeffs b)" |
"coeffs (Mult a b) = mult_coeffs (coeffs a) (coeffs b)"

(* eval over add_coeffs behaves as expected *)
lemma add_coeffs_eval [simp]: "evalp (add_coeffs a b) x = evalp a x + evalp b x"
  apply (induction a arbitrary: b)
  apply simp
  apply (case_tac b)
  apply simp
  apply (simp add: algebra_simps)
  done

(* eval over scale_coeffs behaves as expected *)
lemma scale_coeffs_eval [simp]: "evalp (scale_coeffs x a) z = x * (evalp a z)"
  apply (induction a)
  apply (auto simp add:algebra_simps)
  done

(* eval over mult_coeffs behaves as expected *)
lemma mult_coeffs_eval [simp]: "evalp (mult_coeffs a b) x = evalp a x * evalp b x"
  apply (induction a)
  apply (auto simp add:algebra_simps)
  done

theorem "evalp (coeffs e) x = eval e x"
  apply (induction e)
  apply auto
  done

end