theory "Ex3-12" imports "given/BExp" begin

(* Exercise 3.12. This is a variation on the previous exercise. Let the instruction set be

datatype instr0 = LDI0 val | LD0 vname | MV0 reg | ADD0 reg

All instructions refer implicitly to register 0 as the source (MV0) or target
(all others). Define a compiler pretty much as explained above except that
the compiled code leaves the value of the expression in register 0. Prove that
exec (comp a r ) s rs 0 = aval a s. *)

type_synonym reg = nat
datatype instr0 = LDI0 val | LD0 vname | MV0 reg | ADD0 reg

fun exec01 :: "instr0 \<Rightarrow> state \<Rightarrow> (reg \<Rightarrow> int) \<Rightarrow> reg \<Rightarrow> int" where
"exec01 (LDI0 i)  _ rs = rs(0 := i) " |
"exec01 (LD0  n)  s rs = rs(0 := s n)" |
"exec01 (ADD0 r1) _ rs = rs(0 := (rs 0)+(rs r1))" |
"exec01 (MV0  r1) _ rs = rs(r1 := rs 0)"

fun exec0 :: "instr0 list \<Rightarrow> state \<Rightarrow> (reg \<Rightarrow> int) \<Rightarrow> reg \<Rightarrow> int" where
"exec0 [] _ rs = rs" |
"exec0 (i # is) s rs = exec0 is s (exec01 i s rs)"

fun comp0 :: "aexp \<Rightarrow> reg \<Rightarrow> instr0 list" where
"comp0 (N n) _ = [LDI0 n]" |
"comp0 (V x) _ = [LD0 x]"  |
"comp0 (Plus a1 a2) r = comp0 a1 (r+1) @ [MV0 (r+1)] @ comp0 a2 (r+1) @ [ADD0 (r+1)]"

lemma exec0_append[simp]: "exec0 (is1@is2) s rs = exec0 is2 s (exec0 is1 s rs)"
  apply(induction is1 s rs rule:exec0.induct)
  apply auto
  done

lemma exec0_before[simp]: "r1 > Suc r2 \<Longrightarrow> (exec0 (comp0 a r1) s rs) (Suc r2) = rs (Suc r2)"
  apply (induction a r1 arbitrary:r2 rs s rule:comp0.induct)
  apply auto
  done

lemma exec0_before'[simp]: "(exec0 (comp0 a (Suc r1)) s rs) (Suc r1) = rs (Suc r1)"
  apply (induction a r1 arbitrary:rs s rule:comp0.induct)
  apply auto
  done

theorem "exec0 (comp0 a r) s rs 0 = aval a s"
  apply (induction a r arbitrary:s rs rule:comp0.induct)
  apply auto
  done

end