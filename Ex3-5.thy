theory "Ex3-5" imports "given/AExp" begin

(* Exercise 3.5. Define a datatype aexp2 of extended arithmetic expressions
that has, in addition to the constructors of aexp, a constructor for modelling
a C-like post-increment operation x++, where x must be a variable. Define an
evaluation function aval2 :: aexp2 \<Rightarrow> state \<Rightarrow> val \<times> state that returns both
the value of the expression and the new state. The latter is required because
post-increment changes the state.

Extend aexp2 and aval2 with a division operation. Model partiality of
division by changing the return type of aval2 to (val \<times> state) option. In
case of division by 0 let aval2 return None. Division on int is the infix div. *)

datatype aexp2 = N int | V vname | Plus aexp2 aexp2 | Times aexp2 aexp2 | Inc vname

fun aval2 :: "aexp2 \<Rightarrow> state \<Rightarrow> val \<times> state" where
"aval2 (N x) s = (x, s)" |
"aval2 (V n) s = (s n, s)" |
"aval2 (Plus l r) s1 = (case (aval2 l s1) of (v1, s2)
  \<Rightarrow> (case (aval2 r s2) of (v2, s3)
  \<Rightarrow> (v1+v2, s3)
))" |
"aval2 (Times l r) s1 = (case (aval2 l s1) of (v1, s2)
  \<Rightarrow> (case (aval2 r s2) of (v2, s3)
  \<Rightarrow> (v1*v2, s3)
))" |
"aval2 (Inc n) s = (s n, s(n := (s n + 1)))"



datatype aexp3 = N int | V vname | Plus aexp3 aexp3
  | Times aexp3 aexp3 | Inc vname | Div aexp3 aexp3

fun aval3 :: "aexp3 \<Rightarrow> state \<Rightarrow> (val \<times> state) option" where
"aval3 (N x) s = Some (x, s)" |
"aval3 (V n) s = Some (s n, s)" |
"aval3 (Plus l r) s1 = (case (aval3 l s1) of
  Some(v1, s2) \<Rightarrow> (case (aval3 r s2) of
      Some (v2, s3) \<Rightarrow> Some (v1+v2, s3) |
      None \<Rightarrow> None
  ) |
  None \<Rightarrow> None
)" |
"aval3 (Times l r) s1 = (case (aval3 l s1) of
  Some(v1, s2) \<Rightarrow> (case (aval3 r s2) of
      Some (v2, s3) \<Rightarrow> Some (v1*v2, s3) |
      None \<Rightarrow> None
  ) |
  None \<Rightarrow> None
)" |
"aval3 (Div l r) s1 = (case (aval3 l s1) of
  Some(v1, s2) \<Rightarrow> (case (aval3 r s2) of
      Some (v2, s3) \<Rightarrow> (if (v2=0) then None else (Some (v1 div v2, s3))) |
      None \<Rightarrow> None
  ) |
  None \<Rightarrow> None
)" |
"aval3 (Inc n) s = Some (s n, s(n := (s n + 1)))"

end