theory "Ex3-8" imports "given/BExp" begin
(* Exercise 3.8. Consider an alternative type of boolean expressions featuring
a conditional:

datatype ifexp = Bc2 bool | If ifexp ifexp ifexp | Less2 aexp aexp

First define an evaluation function ifval :: ifexp \<Rightarrow> state \<Rightarrow> bool analogously
to bval. Then define two functions b2ifexp :: bexp \<Rightarrow> ifexp and if2bexp ::
ifexp \<Rightarrow> bexp and prove their correctness, i.e., that they preserve the value
of an expression. *)

datatype ifexp = Bc2 bool | If ifexp ifexp ifexp | Less2 aexp aexp

fun ifval :: "ifexp \<Rightarrow> state \<Rightarrow> bool" where
"ifval (Bc2 v) _ = v" |
"ifval (If b1 b2 b3) s = (if (ifval b1 s) then (ifval b2 s) else (ifval b3 s))" |
"ifval (Less2 a1 a2) s = ((aval a1 s) < (aval a2 s))"

fun b2ifexp :: "bexp \<Rightarrow> ifexp" where
"b2ifexp (Bc v) = Bc2 v" |
"b2ifexp (Not b) = If (b2ifexp b) (Bc2 False) (Bc2 True)" |
"b2ifexp (And b1 b2) = If (b2ifexp b1) (b2ifexp b2) (Bc2 False)" |
"b2ifexp (Less a1 a2) = Less2 a1 a2"

fun if2bexp :: "ifexp \<Rightarrow> bexp" where
"if2bexp (Bc2 v) = Bc v" |
"if2bexp (If i1 i2 i3) = Not (And (Not (And (if2bexp i1) (if2bexp i2))) (Not (And (Not (if2bexp i1)) (if2bexp i3))))" |
"if2bexp (Less2 a1 a2) = Less a1 a2"

theorem "bval b s = ifval (b2ifexp b) s"
  apply (induction b)
  apply auto
  done

theorem "ifval i s = bval (if2bexp i) s"
  apply (induction i)
  apply auto
  done

end