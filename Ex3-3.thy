theory "Ex3-3" imports "given/AExp" begin

(* Exercise 3.3. Substitution is the process of replacing a variable by an expression in an expression.
Define a substitution function subst :: vname \<Rightarrow> aexp \<Rightarrow> aexp \<Rightarrow> aexp
such that subst x a e is the result of replacing every occurrence of
variable x by a in e. For example:

subst ''x'' (N 3) (Plus (V ''x'') (V ''y'')) = Plys (N 3) (V ''y'')

Prove the so-called substitution lemma that says that we can either
substitute first and evaluate afterwards or evaluate with an updated state:
aval (subst x a e) s = aval e (s(x := aval a s)). As a consequence prove
aval a1 s = aval a2 s \<Longrightarrow> aval (subst x a1 e) s = aval (subst x a2 e) s. *)


fun subst :: "vname \<Rightarrow> aexp \<Rightarrow> aexp \<Rightarrow> aexp" where
"subst x1 e (V x2) = (if (x1 = x2) then e else (V x2))" |
"subst _ _ (N x) = N x" |
"subst x1 e (Plus l r) = Plus (subst x1 e l) (subst x1 e r)"

theorem [simp]: "aval (subst x a e) s = aval e (s(x := aval a s))"
  apply (induction e)
  apply auto
  done

theorem "aval a1 s = aval a2 s \<Longrightarrow> aval (subst x a1 e) s = aval (subst x a2 e) s"
  apply (induction a1)
  apply auto
  done

end