theory "Ex5-4" imports Main begin
  
(* Exercise 5.4. Give a structured proof of \<not> ev (Suc (Suc (Suc 0))) by rule
inversions. If there are no cases to be proved you can close a proof immediately
with qed. *)

inductive ev :: "nat \<Rightarrow> bool" where
ev0: "ev 0" |
evSS: "ev n \<Longrightarrow> ev (Suc (Suc n))"

lemma "\<not> ev (Suc (Suc (Suc 0)))"
proof
  assume "ev (Suc (Suc (Suc 0)))"
  then show "False"
  proof cases
    case evSS
    assume "ev (Suc 0)"
    then show "False" by cases
  qed
qed

end