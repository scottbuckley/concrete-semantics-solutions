theory "Ex2-10" imports Main begin

(* Exercise 2.10. Define a datatype tree0 of binary tree skeletons which do not
store any information, neither in the inner nodes nor in the leaves. Define a
function nodes :: tree0 \<Rightarrow> nat that counts the number of all nodes (inner
nodes and leaves) in such a tree. Consider the following recursive function:

fun explode :: "nat \<Rightarrow> tree0 \<Rightarrow> tree0" where
"explode 0 t = t" |
"explode (Suc n) t = explode n (Node t t)"

Find an equation expressing the size of a tree after exploding it (nodes
(explode n t)) as a function of nodes t and n. Prove your equation. You
may use the usual arithmetic operators, including the exponentiation
operator“^”. For example, 2 ^ 2 = 4.

Hint: simplifying with the list of theorems algebra_simps takes care of
common algebraic properties of the arithmetic operators. *)


datatype tree0 =
Leaf | Node tree0 tree0

fun nodes :: "tree0 \<Rightarrow> nat" where
"nodes Leaf = 1" |
"nodes (Node a b) = Suc (nodes a + nodes b)"

fun explode :: "nat \<Rightarrow> tree0 \<Rightarrow> tree0" where
"explode 0 t = t" |
"explode (Suc n) t = explode n (Node t t)"

lemma [simp]: "nodes (explode (Suc n) t) = Suc (2 * nodes(explode n t))"
  apply (induction n arbitrary: t)
  apply auto
  done

lemma "nodes (explode n t) = ((2 ^ n) * (nodes t + 1))  - 1"
  apply (induction n arbitrary: t)
  apply (auto simp add:algebra_simps)
  done

end