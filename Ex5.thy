theory Ex5 imports Big_Step begin

(* 7.1 *)
fun assigned :: "com \<Rightarrow> vname set" where
"assigned SKIP = {}" |
"assigned (Assign v _) = {v}" |
"assigned (Seq c1 c2) = (assigned c1) \<union> (assigned c2)" |
"assigned (If _ c1 c2) = (assigned c1) \<union> (assigned c2)" |
"assigned (While _ c) = assigned c"

lemma "(c, s) \<Rightarrow> t \<Longrightarrow> x \<notin> assigned c \<Longrightarrow> s x = t x"


end