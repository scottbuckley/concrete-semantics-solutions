theory "Ex3-6" imports "given/AExp" begin

(* Exercise 3.6. The following type adds a LET construct to arithmetic expressions:

datatype lexp = Nl int | Vl vname | Plusl lexp lexp | LET vname lexp lexp

The LET constructor introduces a local variable: the value of LET x e1 e2
is the value of e2 in the state where x is bound to the value of e1 in the
original state. Define a function lval :: lexp \<Rightarrow> state \<Rightarrow> int that evaluates
lexp expressions. Remember s(x := i).

Define a conversion inline :: lexp \<Rightarrow> aexp. The expression LET x e1 e2
is inlined by substituting the converted form of e1 for x in the converted form
of e2. See Exercise 3.3 for more on substitution. Prove that inline is correct
w.r.t. evaluation *)

datatype lexp = Nl int | Vl vname | Plusl lexp lexp | LET vname lexp lexp

fun lval :: "lexp \<Rightarrow> state \<Rightarrow> int" where
"lval (Nl v) s = v" |
"lval (Vl n) s = s n" |
"lval (Plusl l r) s = lval l s + lval r s" |
"lval (LET n e1 e2) s = lval e2 (s(n := lval e1 s))"

fun subst :: "vname \<Rightarrow> aexp \<Rightarrow> aexp \<Rightarrow> aexp" where
"subst x1 e (V x2) = (if (x1 = x2) then e else (V x2))" |
"subst _ _ (N x) = N x" |
"subst x1 e (Plus l r) = Plus (subst x1 e l) (subst x1 e r)"
  
fun inline :: "lexp \<Rightarrow> aexp" where
"inline (Nl v) = N v" |
"inline (Vl n) = V n" |
"inline (Plusl l r) = Plus (inline l) (inline r)" |
"inline (LET n e1 e2) = subst n (inline e1) (inline e2)"

lemma subst_aval [simp] : "aval (subst n e1 e2) s = aval e2 (s(n := aval e1 s))"
  apply (induction e2 arbitrary: n e1 s)
  apply auto
  done

theorem "lval l s = aval (inline l) s"
  apply (induction l arbitrary:s)
  apply auto 
  done

end